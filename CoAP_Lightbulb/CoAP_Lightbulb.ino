#include <coap.h>
#include <WiFi.h>
#include <WiFiUdp.h>


/****************************************************************************

This is a demo application to represent a lightbulb in form of a RESTful API.
The device will connect to an WLAN access point and use static IP address
Modify the credentials and IP for your own purpose!

coap://192.168.4.100/light

payload "1": switch off
payload "0": switch on

GPIO pin 32 is used to control the light (see: ledpin)

****************************************************************************/

//WPA2 credentials
const char* ssid     = "myssid";
const char* password = "mypassword";


IPAddress local_IP(192, 168, 4, 100);
IPAddress gateway(192, 168, 4, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(8, 8, 8, 8);      //optional
IPAddress secondaryDNS(8, 8, 4, 4);    //optional

// CoAP client response callback
void callback_response(CoapPacket &packet, IPAddress ip, int port);

// CoAP server endpoint url callback
void callback_light(CoapPacket &packet, IPAddress ip, int port);

// UDP and CoAP class
WiFiUDP udp;
Coap coap(udp);

// LED STATE
bool LEDSTATE;

int ledpin = 32;

// CoAP server endpoint URL
void callback_light(CoapPacket &packet, IPAddress ip, int port) {
  Serial.println("[Light] ON/OFF");
  
  // send response
  char p[packet.payloadlen + 1];
  memcpy(p, packet.payload, packet.payloadlen);
  p[packet.payloadlen] = NULL;
  
  String message(p);

  if (message.equals("0"))    //string "0" defined as message to switch off LED
    LEDSTATE = false;
  else if(message.equals("1"))  //LED control is high active
    LEDSTATE = true;
      
  if (LEDSTATE) {
    digitalWrite(ledpin, HIGH) ; 
    coap.sendResponse(ip, port, packet.messageid, "1");
  } else { 
    digitalWrite(ledpin, LOW) ; 
    coap.sendResponse(ip, port, packet.messageid, "0");
  }
}

// CoAP client response callback
void callback_response(CoapPacket &packet, IPAddress ip, int port) {
  Serial.println("[Coap Response got]");
  
  char p[packet.payloadlen + 1];
  memcpy(p, packet.payload, packet.payloadlen);
  p[packet.payloadlen] = NULL;
  
  Serial.println(p);
}

void setup() {
  Serial.begin(9600);


  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
    Serial.println("STA Failed to configure");
  }

  // connect to wifi
  WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

  Serial.print("My IP address: ");
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    Serial.print(WiFi.localIP()[thisByte], DEC);
    Serial.print(".");
  }
  Serial.println();

  // LED State
  pinMode(ledpin, OUTPUT);
  digitalWrite(ledpin, LOW);
  LEDSTATE = true;
  
  // add server url endpoints.
  // can add multiple endpoint urls
  Serial.println("Setup Callback Light");
  coap.server(callback_light, "light");

  // client response callback.
  // this endpoint is single callback.
  Serial.println("Setup Response Callback");
  coap.response(callback_response);

  // start coap server/client
  coap.start();
}

void loop() {
  delay(1000);
  coap.loop();
}

